*** Settings ***
Documentation     A test suite for adding a product to the shopping card.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../keywords/resource.robot
Suite Setup       Open Browser And Go To Demo Opencart Page
Suite Teardown    Close Browser


*** Test Cases ***
Adding a product to the shopping card
	Change Currency To GBP
	Search For A Product    iPod
	Add All IPods Returned In Search Results To Product Comparison
	View Product Comparison Page
	Remove The One That Is 'Out Of Stock' From Comparison
	Add A Random Available One To Shopping Cart
	Go To Shopping Cart And Verify That Total Price Matches The One From Comparison Page For Selected Product