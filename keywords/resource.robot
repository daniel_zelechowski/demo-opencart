*** Settings ***
Documentation     A resource file with reusable keywords and variables.

Library           Selenium2Library
Library           String
Library           Collections
Library           DebugLibrary

*** Variables ***
${url}            http://demo.opencart.com/
${browser}        Firefox
${delay}          1 second

*** Keywords ***
Open Browser And Go To Demo Opencart Page
	Open Browser    ${url}    ${browser}
	Maximize Browser Window
	Set Selenium Speed    ${delay}

	Title Should Be    Your Store
	Element Should Be Visible    css=body.common-home

Change Currency To GBP
	Click Element    css=form#currency
	Click Element    css=button[name=GBP]

	Element Should Be Visible    xpath=//form[@id='currency']/div/button/strong[contains(text(),'£')]

Search For A Product
	[Arguments]    ${product}

	Input Text    css=input[placeholder=Search]    ${product}
	Click Element    css=i.fa-search

	Title Should Be    Search - iPod
	Element Should Be Visible    css=.product-grid

Add All IPods Returned In Search Results To Product Comparison
	${products_list} =    Get Webelements    css=button[data-original-title="Compare this Product"]
	${products_quantity} =    Get Length    ${products_list}

	:FOR    ${product}    IN    @{products_list}
	\    Click Element    ${product}

	Page Should Contain    Product Compare (${products_quantity})

View Product Comparison Page
	Click Element    css=a#compare-total

	Title Should Be    Product Comparison
	Element Should Be Visible    css=table.table-bordered

Remove The One That Is 'Out Of Stock' From Comparison
	${product_row} =    Get Webelements    xpath=//div[@id='content']/table/tbody[1]/tr[1]/td
	${number_of_columns} =   Get Length    ${product_row}
	${number_of_last_column_without_offset} =    evaluate    ${number_of_columns} + 1
	${avability_values_list} =    Create List

	:FOR    ${index}    IN RANGE    1    ${number_of_last_column_without_offset}
	\    ${avability_value} =   Get Text    xpath=//div[@id='content']/table/tbody[1]/tr[6]/td[${index}]
	\    Append To List    ${avability_values_list}    ${avability_value}
	\    ${get_index_of_unavailable_product}    Get Index From List    ${avability_values_list}    Out Of Stock
	\    ${position_of_unavailable_product}    evaluate    ${get_index_of_unavailable_product} + 1

	click element    xpath=//div[@id='content']/table/tbody[2]/tr/td[${position_of_unavailable_product}]/a[contains(text(),'Remove')]

	${product_has_been_removed} =    Run keyword and ignore error    Page Should Contain    Success: You have modified your product comparison!
	${update_number_of_columns} =    evaluate    ${number_of_columns} - 1

	Run Keyword If    '${product_has_been_removed[0]}' == 'PASS'
	...    Set Test Variable    ${number_of_remaining_columns}    ${update_number_of_columns}

	${product_row_updated} =    Get Webelements    xpath=//div[@id='content']/table/tbody[1]/tr[1]/td
	${number_of_columns_updated} =   Get Length    ${product_row_updated}

	Should Be Equal As Numbers    ${number_of_remaining_columns}    ${number_of_columns_updated}

Add A Random Available One To Shopping Cart
	${selected_column} =    Evaluate    random.randint(2, ${number_of_remaining_columns})    modules=random
	Set Test Variable    ${selected_product}    ${selected_column}

	Click Element    xpath=//div[@id='content']/table/tbody[2]/tr/td[${selected_product}]/input[@value='Add to Cart']

	Page Should Contain    Success: You have added
	Element Should Be Visible    xpath=//div[@id='cart']/button/span[contains(text(),'1 item(s)')]

Go To Shopping Cart And Verify That Total Price Matches The One From Comparison Page For Selected Product
	${comparison_page_product_price} =   Get Text    xpath=//div[@id='content']/table/tbody[1]/tr[3]/td[${selected_product}]

	Click Element    css=a[title='Shopping Cart']

	Title Should Be    Shopping Cart
	Element Should Be Visible    css=body.checkout-cart

	${shopping_cart_total_price} =    Get Text    xpath=//div[@id='content']/form/div/table/tbody/tr/td[6]

	Should Be Equal    ${comparison_page_product_price}    ${shopping_cart_total_price}